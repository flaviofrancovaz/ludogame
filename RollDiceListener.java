package superludo;
import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class RollDiceListener implements ActionListener
{
	Component comp;
	
	private Image DiceOne;
	private Image DiceTwo;
	private Image DiceThree;
	private Image DiceFour;
	private Image DiceFive;
	private Image DiceSix;
	
	public RollDiceListener (JButton RollDiceButton)
	{
		comp = RollDiceButton;
		
		LoadDiceImages();
	}
	
	public void LoadDiceImages()
	{
		try 
		{
			 DiceOne   = ImageIO.read(new File("C:\\Users\\Flavio\\Documents\\PUC - 2018.2\\POO\\Project\\Dices\\Dado1.png"));
			 DiceTwo   = ImageIO.read(new File("C:\\Users\\Flavio\\Documents\\PUC - 2018.2\\POO\\Project\\Dices\\Dado2.png"));
			 DiceThree = ImageIO.read(new File("C:\\Users\\Flavio\\Documents\\PUC - 2018.2\\POO\\Project\\Dices\\Dado3.png"));
			 DiceFour  = ImageIO.read(new File("C:\\Users\\Flavio\\Documents\\PUC - 2018.2\\POO\\Project\\Dices\\Dado4.png"));
			 DiceFive  = ImageIO.read(new File("C:\\Users\\Flavio\\Documents\\PUC - 2018.2\\POO\\Project\\Dices\\Dado5.png"));
			 DiceSix   = ImageIO.read(new File("C:\\Users\\Flavio\\Documents\\PUC - 2018.2\\POO\\Project\\Dices\\Dado6.png"));
		}
		catch(IOException e) 
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void actionPerformed(ActionEvent e)
	{
		int randomNumber = new Random().nextInt(5);
		randomNumber++;
		
		int topX = 45;
		int topY = 395;
		
		if (randomNumber == 1)
		{	
			//graphics.drawImage(DiceOne, topX, topY, null);
			System.out.println("1");
		}
		else if (randomNumber == 2)
		{
			//graphics.drawImage(DiceTwo, topX, topY, null);
			System.out.println("2");
		}
		else if (randomNumber == 3)
		{
			//graphics.drawImage(DiceThree, topX, topY, null);
			System.out.println("3");
		}
		else if (randomNumber == 4)
		{
			//graphics.drawImage(DiceFour, topX, topY, null);
			System.out.println("4");
		}
		else if (randomNumber == 5)
		{
			//graphics.drawImage(DiceFive, topX, topY, null);
			System.out.println("5");
		}
		else
		{
			//graphics.drawImage(DiceSix, topX, topY, null);
			System.out.println("6");
		}
	}
}
