package superludo;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.*;

import javax.swing.JPanel;

public class BoardPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	public BoardPanel()
	{
		setVisible(true);
		setLayout(null);
		setBounds(0, 0, 900, 900);
		setBackground(Color.WHITE);
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		// Drawing red side of the board
		g2d.setPaint(new Color(255, 51, 51));
		g2d.fill(new Rectangle2D.Double(0, 0, 300, 300));
		
		g2d.fill(new Rectangle2D.Double(50, 300, 50, 100));
		g2d.fill(new Rectangle2D.Double(50, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(100, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(150, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(200, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(250, 400, 50, 100));
		
		g2d.fillPolygon(new int[] {300, 450, 300}, new int[] {300, 450, 600} , 3);
		
		// Drawing green side of the board
		g2d.setPaint(new Color(132, 225, 132));
		g2d.fill(new Rectangle2D.Double(600, 0, 300, 300));
		
		g2d.fill(new Rectangle2D.Double(500, 50, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 50, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 100, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 150, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 200, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 250, 100, 50));
		
		g2d.fillPolygon(new int[] {300, 450, 600}, new int[] {300, 450, 300} , 3);
		
		// Drawing yellow side of the board
		g2d.setPaint(new Color(230, 230, 0));
		g2d.fill(new Rectangle2D.Double(600, 600, 300, 300));
		
		g2d.fill(new Rectangle2D.Double(800, 500, 50, 100));
		g2d.fill(new Rectangle2D.Double(800, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(750, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(700, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(650, 400, 50, 100));
		g2d.fill(new Rectangle2D.Double(600, 400, 50, 100));
		
		g2d.fillPolygon(new int[] {450, 600, 600}, new int[] {450, 300, 600} , 3);
		
		// Drawing blue side of the board
		g2d.setColor(new Color(102, 163, 255));
		g2d.fill(new Rectangle2D.Double(0, 600, 300, 300));
		
		g2d.fill(new Rectangle2D.Double(300, 800, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 800, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 750, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 700, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 650, 100, 50));
		g2d.fill(new Rectangle2D.Double(400, 600, 100, 50));
		
		g2d.fillPolygon(new int[] {300, 450, 600}, new int[] {600, 450, 600} , 3);
		
		g2d.setColor(Color.WHITE);
		
		// Drawing entrance houses triangles
		g2d.fillPolygon(new int[] {55, 95, 55}, new int[] {330, 350, 370} , 3);
		g2d.fillPolygon(new int[] {530, 550, 570}, new int[] {55, 95, 55} , 3);
		g2d.fillPolygon(new int[] {330, 350, 370}, new int[] {845, 805, 845} , 3);
		g2d.fillPolygon(new int[] {845, 805, 845}, new int[] {530, 550, 570} , 3);
		
		// Drawing white circles inside big squares
		Ellipse2D circ = new Ellipse2D.Double();
		
		circ.setFrameFromCenter(75, 75, 75 + 35, 75 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(225, 75, 225 + 35, 75 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(75, 225, 75 + 35, 225 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(225, 225, 225 + 35 , 225 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(675, 75, 675 + 35, 75 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(825, 75, 825 + 35, 75 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(675, 225, 675 + 35, 225 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(825, 225, 825 + 35, 225 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(75, 675, 75 + 35, 675 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(225, 675, 225 + 35, 675 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(75, 825, 75 + 35, 825 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(225, 825, 225 + 35, 825 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(675, 675, 675 + 35, 675 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(825, 675, 825 + 35, 675 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(675, 825, 675 + 35, 825 + 35);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(825, 825, 825 + 35, 825 + 35);
		g2d.fill(circ);
		
		
		g2d.setPaint(Color.BLACK);
		// Drawing black squares
		g2d.fill(new Rectangle2D.Double(300, 50, 100, 50));
		g2d.fill(new Rectangle2D.Double(500, 800, 100, 50));
		g2d.fill(new Rectangle2D.Double(50, 500, 50, 100));
		g2d.fill(new Rectangle2D.Double(800, 300, 50, 100));
		
		// Drawing Edges
		g2d.draw(new Rectangle2D.Double(0, 0, 300, 300));
		g2d.drawPolygon(new int[] {300, 450, 300}, new int[] {300, 450, 600} , 3);
		
		g2d.draw(new Rectangle2D.Double(600, 0, 300, 300));
		g2d.drawPolygon(new int[] {300, 450, 600}, new int[] {300, 450, 300} , 3);
		
		g2d.draw(new Rectangle2D.Double(600, 600, 300, 300));
		g2d.drawPolygon(new int[] {450, 600, 600}, new int[] {450, 300, 600} , 3);
		
		g2d.draw(new Rectangle2D.Double(0, 600, 300, 300));
		g2d.drawPolygon(new int[] {300, 450, 600}, new int[] {600, 450, 600} , 3);
		
		g2d.draw(new Rectangle2D.Double(0, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(0, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(0, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(100, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(150, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(200, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(250, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(100, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(150, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(200, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(250, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(600, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(650, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(700, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(750, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(850, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(850, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(600, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(650, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(700, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(750, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(850, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(50, 300, 50, 100));
		g2d.draw(new Rectangle2D.Double(50, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(100, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(150, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(200, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(250, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(600, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(650, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(700, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(750, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(800, 400, 50, 100));
		g2d.draw(new Rectangle2D.Double(800, 500, 50, 100));
		g2d.draw(new Rectangle2D.Double(300, 0, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 0, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 0, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 100, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 100, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 150, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 150, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 200, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 200, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 250, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 250, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 600, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 600, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 650, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 650, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 700, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 700, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 750, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 750, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 850, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 850, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 850, 100, 50));				
		g2d.draw(new Rectangle2D.Double(400, 50, 100, 50));
		g2d.draw(new Rectangle2D.Double(500, 50, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 100, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 150, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 200, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 250, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 600, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 650, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 700, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 750, 100, 50));
		g2d.draw(new Rectangle2D.Double(400, 800, 100, 50));
		g2d.draw(new Rectangle2D.Double(300, 800, 100, 50));
		
		circ.setFrameFromCenter(75, 75, 75 + 35, 75 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(225, 75, 225 + 35, 75 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(75, 225, 75 + 35, 225 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(225, 225, 225 + 35 , 225 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(675, 75, 675 + 35, 75 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(825, 75, 825 + 35, 75 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(675, 225, 675 + 35, 225 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(825, 225, 825 + 35, 225 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(75, 675, 75 + 35, 675 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(225, 675, 225 + 35, 675 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(75, 825, 75 + 35, 825 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(225, 825, 225 + 35, 825 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(675, 675, 675 + 35, 675 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(825, 675, 825 + 35, 675 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(675, 825, 675 + 35, 825 + 35);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(825, 825, 825 + 35, 825 + 35);
		g2d.draw(circ);
	}
}
