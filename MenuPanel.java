package superludo;
import java.awt.Color;
import java.awt.geom.*;
import java.awt.*;
import javax.swing.*;

public class MenuPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	private static JButton NewGameButton  = new JButton("Novo Jogo");
	private static JButton LoadGameButton = new JButton("Carregar Jogo");
	private static JButton SaveGameButton = new JButton("Salvar Jogo");	
	
	private static JButton RollDiceButton = new JButton("Lan�ar Dado"); 
	private static JLabel Label = new JLabel("A Jogar:");
	
	public MenuPanel() 
	{			
		setVisible(true);
		setLayout(null);
		setBounds(900, 0, 200, 900);
		setBackground(new Color(117, 117, 163));
		
		NewGameButton.setBounds(30, 50, 130, 30);
		NewGameButton.setBackground(new Color(89, 0, 179));
		NewGameButton.setForeground(Color.WHITE);
		
		LoadGameButton.setBounds(30, 120, 130, 30);
		LoadGameButton.setBackground(new Color(89, 0, 179));
		LoadGameButton.setForeground(Color.WHITE);
		
		SaveGameButton.setBounds(30, 190, 130, 30);
		SaveGameButton.setBackground(new Color(89, 0, 179));
		SaveGameButton.setForeground(Color.WHITE);
		
		add(NewGameButton);
		add(LoadGameButton);
		add(SaveGameButton);
		
		Label.setBounds(30, 260, 130, 30);
		Label.setForeground(Color.WHITE);				
		
		add(Label);
		
		RollDiceButton.setBounds(30,  530, 130, 30);
		RollDiceButton.setBackground(new Color(89, 0, 179));
		RollDiceButton.setForeground(Color.WHITE);
		RollDiceButton.addActionListener(new RollDiceListener(RollDiceButton));
		
		add(RollDiceButton);
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		double topX = 30;
		double topY = 380;
		
		double squareWidth  = 130;
		double squareHeight = 130;
		
		Graphics2D g2d = (Graphics2D)g;
		
		Rectangle2D rec = new Rectangle2D.Double(topX, topY, squareWidth, squareHeight);
		
		g2d.draw(rec);
		
		g2d.draw(new Rectangle2D.Double(0, 0, 200, 900));
	}
}
