package super_ludo;

import java.awt.Color;
import java.io.Serializable;

final class House implements Serializable
{
	private static final long serialVersionUID = 42L;
	public final int X;
	public final int Y;
	
	public House(int x, int y)
	{
		X = x;
		Y = y;
	}
}

public class Pawn implements Serializable
{	
	private static final long serialVersionUID = 42L;
	private int house;
	private int X;
	private int Y;
	private int Xhome;
	private int Yhome;
	private Color color;
	//private int player;
	
	public Pawn(int x, int y, Color color)
	{
		this.color = color;
		house = -6;
		Xhome = x;
		Yhome = y;
		X = Xhome;
		Y = Yhome;
	}
	
	public void move(int dice, BoardPanel b)
	{
		int i,rest,player;
		
		if(color == MainFrame.RedPlayer)
			player = 1;			
		else if(color == MainFrame.GreenPlayer) 
			player = 2;			
		else if(color == MainFrame.YellowPlayer)
			player = 3;			
		else if(color == MainFrame.BluePlayer) 
			player = 4;
		else
			player = 0;
			
		if(house != 51)
		{
			if(house == -6 && dice == 5) 
				leaveHome();
			else if(house != -6)
			{
				house += dice;
				
				if(house > 51)
					house = 51 - house%51;
				
				if(house <= -6)
					returnHome();
				else
				{											
					if(house < 0)
						i = 0;
					else
						i = (int)Math.ceil(house/13) + 1;
										
					rest = house%13;

					switch(player)
					{
						case(1):
							switch(i)
							{
								case(0):
									X = BoardPanel.initials.get(color).X + (int)((house + 5.5)*BoardPanel.sideHouses);
									break;
								case(1):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)(5.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)((rest + 0.5)*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X + (int)(6.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(5.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)(7.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((rest - 12.5)*BoardPanel.sideHouses);
									}
									break;
								case(2):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)((8.5 + rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(0.5*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X + (int)(13.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(1.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((20.5 - rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(2.5*BoardPanel.sideHouses);
									}
									break;
								case(3):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)(7.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((3.5 + rest)*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X + (int)(6.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(8.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)(5.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((15.5 - rest)*BoardPanel.sideHouses);
									}
									break;
								case(4):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)((4.5 - rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(2.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((rest - 6.5)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(1.5*BoardPanel.sideHouses);
									}
									break;
							}
							break;
						case(2):
							switch(i)
							{
								case(0):
									Y = BoardPanel.initials.get(color).Y + (int)((house + 5.5)*BoardPanel.sideHouses);
									break;
								case(1):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)((1.5 + rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(5.5*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X + (int)(6.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(6.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((13.5 - rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(7.5*BoardPanel.sideHouses);
									}
									break;
								case(2):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)(0.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((8.5 + rest)*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X - (int)(0.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(13.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X - (int)(1.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((20.5 - rest)*BoardPanel.sideHouses);
									}
									break;
								case(3):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X - (int)((2.5 + rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(7.5*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X - (int)(7.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(6.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((rest - 14.5)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(5.5*BoardPanel.sideHouses);
									}
									break;
								case(4):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X - (int)(1.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((4.5 - rest)*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X - (int)(0.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((rest - 6.5)*BoardPanel.sideHouses);
									}
									break;
							}
							break;
						case(3):
							switch(i)
							{
								case(0):
									X = BoardPanel.initials.get(color).X - (int)((house + 4.5)*BoardPanel.sideHouses);
									break;
								case(1):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X - (int)(4.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((rest + 1.5)*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X - (int)(5.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(6.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X - (int)(6.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((13.5 - rest)*BoardPanel.sideHouses);
									}
									break;
								case(2):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X - (int)((7.5 + rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)(0.5*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X - (int)(12.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(0.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((rest - 19.5)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(1.5*BoardPanel.sideHouses);
									}
									break;
								case(3):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X - (int)(6.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)((2.5 + rest)*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X - (int)(5.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(7.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X - (int)(4.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((rest - 14.5)*BoardPanel.sideHouses);
									}
									break;
								case(4):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)((rest - 3.5)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(1.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((7.5 - rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(0.5*BoardPanel.sideHouses);
									}
									break;
							}
							break;
						case(4):
							switch(i)
							{
								case(0):
									Y = BoardPanel.initials.get(color).Y - (int)((house + 4.5)*BoardPanel.sideHouses);
									break;
								case(1):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X - (int)((rest + 0.5)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(4.5*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X - (int)(5.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(5.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((rest - 12.5)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(6.5*BoardPanel.sideHouses);
									}
									break;
								case(2):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)(0.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)((7.5 + rest)*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X + (int)(1.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(12.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)(2.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((rest - 19.5)*BoardPanel.sideHouses);
									}
									break;
								case(3):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)((3.5 + rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(6.5*BoardPanel.sideHouses);
									}
									else if(rest == 6)
									{
										X = BoardPanel.initials.get(color).X + (int)(8.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(5.5*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)((15.5 - rest)*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y - (int)(4.5*BoardPanel.sideHouses);
									}
									break;
								case(4):
									if(rest <= 5)
									{
										X = BoardPanel.initials.get(color).X + (int)(2.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((rest - 3.5)*BoardPanel.sideHouses);
									}
									else
									{
										X = BoardPanel.initials.get(color).X + (int)(1.5*BoardPanel.sideHouses);
										Y = BoardPanel.initials.get(color).Y + (int)((7.5 - rest)*BoardPanel.sideHouses);
									}
									break;
							}
							break;
					}
				}
			}
		}
	}
	
	public void returnHome()
	{		
		house = -6;
		X = Xhome;
		Y = Yhome;
	}
	
	public void leaveHome()
	{
		house = -5;
		X = BoardPanel.initials.get(color).X + (int)(0.5*BoardPanel.sideHouses);
		Y = BoardPanel.initials.get(color).Y + (int)(0.5*BoardPanel.sideHouses);
	}
	
	public boolean isAbleToMove(int dice, BoardPanel b)
	{
		int x,y,
			pawnIn,pawnOut;
		Pawn substitute;
		
		if(this.isHome())
		{
			if(dice != 5 || !b.isAbleToLeave(color))
				return false;
		}
		else if(dice == 5 &&  b.getNumberOfPawnsAtHome(color) > 0 && b.isAbleToLeave(color))
			return false;
		else if(house == 51)
			return false;
		else
		{
			substitute = new Pawn(Xhome,Yhome,color);
			substitute.move(5, b);
			substitute.move(house + 5, b);
			
			x = X/BoardPanel.sideHouses;
			y = Y/BoardPanel.sideHouses;
			
			for(int j = 0; j <dice-1; j++)
			{
				substitute.move(1, b);
				x = substitute.getX()/BoardPanel.sideHouses;
				y = substitute.getY()/BoardPanel.sideHouses;
				
				if(b.grid[x][y]%100 != 0)
				{
					pawnIn  = b.grid[x][y]%100 - 1;
					pawnOut = (b.grid[x][y] - (pawnIn + 1))/100 - 1;
					
					if(b.pawns[pawnIn].getPlayer() == b.pawns[pawnOut].getPlayer() && b.pawns[pawnIn].getPlayer() != color)
					{
						return false;
					}		
				}
			}
				
			substitute.move(1, b);
			x = substitute.getX()/BoardPanel.sideHouses;
			y = substitute.getY()/BoardPanel.sideHouses;
			
			if(b.grid[x][y]%100 != 0)
				return false;
		}
		
		return true;
	}
	
	public boolean isHome()
	{
		if(X == Xhome && Y == Yhome)
			return true;
		
		return false;
	}
	
	public int getX()
	{
		return X;		
	}
	
	public int getY()
	{		
		return Y;		
	}
	
	public int getXhome()
	{
		return Xhome;		
	}
	
	public int getYhome()
	{		
		return Yhome;		
	}
	
	public int getHouse()
	{
		return house;
	}
	
	public Color getPlayer()
	{
		return color;
	}
}
