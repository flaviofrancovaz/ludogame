package super_ludo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.JFrame;

public class MainFrame extends JFrame implements Serializable
{
	private static final long serialVersionUID = 42L;
	public final int LARG_DEFAULT	 = 19*BoardPanel.sideHouses+1;
	public final int ALT_DEFAULT	 = 15*BoardPanel.sideHouses+1 + 28;
	
	public static final transient Color RedHouse     = new Color(255, 51, 51);
	public static final transient Color GreenHouse   = new Color(132, 225, 132);
	public static final transient Color YellowHouse  = new Color(230, 230, 0);
	public static final transient Color BlueHouse    = new Color(102, 163, 255);
	public static final transient Color houses[] = { RedHouse, BlueHouse, YellowHouse, GreenHouse};
	
	public static final transient Color BlueMenu = new Color(117, 117, 163);
	
	public static final transient Color RedPlayer    = new Color(200, 0, 0);
	public static final transient Color GreenPlayer  = new Color(0, 120, 0);
	public static final transient Color YellowPlayer = new Color(170, 170, 0);
	public static final transient Color BluePlayer   = Color.BLUE;
	public static final transient Color players[] = { RedPlayer, GreenPlayer, YellowPlayer, BluePlayer, BlueMenu};
		
	private static MainFrame Main = null;
	private int currentPlayer = 4;
	
	public MenuPanel Menu;   
	public BoardPanel Board;
	public ScorePanel Score;
	
	public static MainFrame getMainFrame() 
	{
		if(Main == null)
			Main = new MainFrame();
		
		return Main;
	}
	
	private MainFrame()
	{
		Score = new ScorePanel(this);
		Menu = new MenuPanel(this);
		Board = new BoardPanel(this);
		
		setLayout(null);
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension screenSize = tk.getScreenSize();
		
		int sl = screenSize.width;
		int sa = screenSize.height;
		
		int x = sl/2 - LARG_DEFAULT/2;
		int y = sa/2 - ALT_DEFAULT/2;
		
		setBounds(x, y, LARG_DEFAULT,ALT_DEFAULT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Super Ludo");
		setVisible(true);
		setResizable(true);
				
		getContentPane().add(Score);
		getContentPane().add(Menu);
		getContentPane().add(Board);
	}
	
	public void nextPlayer(int dice, char repeat)
	{
		if(dice != 6 || repeat == '2')
		{
			currentPlayer++;
			currentPlayer %= 4;
		}
	}
	
	public void setInitialPlayer()
	{
		currentPlayer = new Random().nextInt(4);
	}
	
	public Color getCurrentPlayer()
	{
		return players[currentPlayer];
	}
	
	public void restart()
	{
		currentPlayer = 4;
		
		Board.resetPawns(Board);
		
		for(int i = 0; i <15; i++)
		{
			for(int j = 0; j < 15; j++)
				Board.grid[i][j] = 0;
		}
		
		Board.grid[1][1]   = 100;
		Board.grid[4][1]   = 200;
		Board.grid[1][4]   = 300;
		Board.grid[4][4]   = 400;
		
		Board.grid[10][1]  = 500;
		Board.grid[13][1]  = 600;
		Board.grid[10][4]  = 700;
		Board.grid[13][4]  = 800;
		
		Board.grid[10][10] = 900;
		Board.grid[13][10] = 1000;
		Board.grid[10][13] = 1100;
		Board.grid[13][13] = 1200;		
		           
		Board.grid[1][10]  = 1300;
		Board.grid[4][10]  = 1400;
		Board.grid[1][13]  = 1500;
		Board.grid[4][13]  = 1600;
	}
	
	public static void LoadGame(ObjectInputStream o)
	{
		try
		{
			Main = (MainFrame) o.readObject();
		}
		catch(ClassNotFoundException ex) 
		{
			ex.printStackTrace();
		}
		catch(IOException ex) 
		{
			System.out.println("Error initializing stream");
		} 
	}
}
