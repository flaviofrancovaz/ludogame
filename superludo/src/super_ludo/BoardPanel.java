package super_ludo;

import super_ludo.MainFrame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
//import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.*;
import java.io.Serializable;

import javax.swing.JPanel;

public class BoardPanel extends JPanel implements Serializable
{
	private static final long serialVersionUID = 42L;
	
	public static final int  sideHouses     = 50;
	public static final int  diametreHomes  = (int)(sideHouses*0.7);
	public static final int  diametrePawns  = (int)(sideHouses*0.4);
	
	public  static final Map<Color,House> initials = new HashMap<Color,House>();
	{
		initials.put(MainFrame.RedPlayer,    new House(1*sideHouses,  6*sideHouses));
		initials.put(MainFrame.GreenPlayer,  new House(8*sideHouses,  1*sideHouses));
		initials.put(MainFrame.YellowPlayer, new House(13*sideHouses, 8*sideHouses));
		initials.put(MainFrame.BluePlayer,   new House(6*sideHouses,  13*sideHouses));
	}	
	
	public static final Map<Color,House> neutrals = new HashMap<Color,House>();
	{
		neutrals.put(MainFrame.RedPlayer,    new House(1*sideHouses,  8*sideHouses));
		neutrals.put(MainFrame.GreenPlayer,  new House(6*sideHouses,  1*sideHouses));
		neutrals.put(MainFrame.YellowPlayer, new House(13*sideHouses, 6*sideHouses));
		neutrals.put(MainFrame.BluePlayer,   new House(8*sideHouses,  13*sideHouses));
	}
	
	public Pawn pawns[]   = new Pawn[16];
	public int  grid[][]  = new int[15][15];
	public int  lastPawn  = -1;
	public Map<Color,Integer> pawnsAtHome  = new HashMap<Color,Integer>();
	public Map<Color,Integer> pawnsAtFinal = new HashMap<Color,Integer>();
	public List<Integer> 	  fullHouses   = new ArrayList<>();
	public Map<Color,int[]>   barriers     = new HashMap<Color,int[]>();
	public Map<Color,Integer> scores       = new HashMap<Color,Integer>(); 
		
	public BoardPanel(MainFrame m)
	{
		setVisible(true);
		setLayout(null);	
		setBounds(0, 0, 15*sideHouses+1, 15*sideHouses+1);
		setBackground(Color.WHITE);
		//updateScore();
		
		// initializing every player with none pawns at final house or barriers and all theirs pawns at initial house 
		for(int i = 0; i < 4; i++)
		{
			barriers.put(MainFrame.players[i], new int[]{0,0});
			pawnsAtHome.put(MainFrame.players[i], 4);
			pawnsAtFinal.put(MainFrame.players[i], 0);
		}
		
		// initialing grid
		for(int i = 0; i <15; i++)
			for(int j = 0; j < 15; j++)
				grid[i][j] = 0;
		
		// initializing pawns
		//red pawns
		pawns[0]   = new Pawn((int)(1.5*sideHouses), (int)(1.5*sideHouses), MainFrame.RedPlayer);
		pawns[1]   = new Pawn((int)(4.5*sideHouses), (int)(1.5*sideHouses), MainFrame.RedPlayer);
		pawns[2]   = new Pawn((int)(1.5*sideHouses), (int)(4.5*sideHouses), MainFrame.RedPlayer);
		pawns[3]   = new Pawn((int)(4.5*sideHouses), (int)(4.5*sideHouses), MainFrame.RedPlayer);
	    
		//green pawns
		pawns[4]   = new Pawn((int)(10.5*sideHouses), (int)(1.5*sideHouses), MainFrame.GreenPlayer);
		pawns[5]   = new Pawn((int)(13.5*sideHouses), (int)(1.5*sideHouses), MainFrame.GreenPlayer);
		pawns[6]   = new Pawn((int)(10.5*sideHouses), (int)(4.5*sideHouses), MainFrame.GreenPlayer);
		pawns[7]   = new Pawn((int)(13.5*sideHouses), (int)(4.5*sideHouses), MainFrame.GreenPlayer);
		             
		//yellow pawns
		pawns[8]   = new Pawn((int)(10.5*sideHouses), (int)(10.5*sideHouses), MainFrame.YellowPlayer);
		pawns[9]   = new Pawn((int)(13.5*sideHouses), (int)(10.5*sideHouses), MainFrame.YellowPlayer);
		pawns[10]  = new Pawn((int)(10.5*sideHouses), (int)(13.5*sideHouses), MainFrame.YellowPlayer);
		pawns[11]  = new Pawn((int)(13.5*sideHouses), (int)(13.5*sideHouses), MainFrame.YellowPlayer);
		
		//blue pawns
		pawns[12]  = new Pawn((int)(1.5*sideHouses), (int)(10.5*sideHouses), MainFrame.BluePlayer);
		pawns[13]  = new Pawn((int)(4.5*sideHouses), (int)(10.5*sideHouses), MainFrame.BluePlayer);
		pawns[14]  = new Pawn((int)(1.5*sideHouses), (int)(13.5*sideHouses), MainFrame.BluePlayer);
		pawns[15]  = new Pawn((int)(4.5*sideHouses), (int)(13.5*sideHouses), MainFrame.BluePlayer);
		
		// initializing pawns places in grid		
		grid[1][1]   = 100;
		grid[4][1]   = 200;
		grid[1][4]   = 300;
		grid[4][4]   = 400;
		grid[10][1]  = 500;
		grid[13][1]  = 600;
		grid[10][4]  = 700;
		grid[13][4]  = 800;
		grid[10][10] = 900;
		grid[13][10] = 1000;
		grid[10][13] = 1100;
		grid[13][13] = 1200;
		grid[1][10]  = 1300;
		grid[4][10]  = 1400;
		grid[1][13]  = 1500;
		grid[4][13]  = 1600;
		
		this.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				int x,y,
					dice,
					pawnSelected;
				Color currentPlayer;
				
				// board only is able to do something only if the RollDiceButton is unable
				if(!m.Menu.RollDiceButton.isEnabled())
				{
					x = (int)Math.floor(e.getX()/sideHouses);
					y = (int)Math.floor(e.getY()/sideHouses);
					dice = m.Menu.getDice();
					currentPlayer = m.getCurrentPlayer();	
					
					// the selected house has a pawn
					if(grid[x][y] != 0)
					{
						pawnSelected = (grid[x][y] - grid[x][y]%100)/100 - 1;
						
						// check when house clicked has more then one pawn selected a pawn that belong to the current player if there is more then one pawn
						if(pawns[pawnSelected].getPlayer() != currentPlayer && grid[x][y]%100 != 0)
						{
							pawnSelected = grid[x][y]%100 -1;
							grid[x][y]   = (grid[x][y]%100)*100 + (grid[x][y] - grid[x][y]%100)/100;
						}
						
						if(pawns[pawnSelected].getPlayer() == currentPlayer)
							movePawn(currentPlayer, pawnSelected, dice, m);					
					}
				}
			}	
		});
	}
	
	boolean rollSix(MainFrame m, Color player, int dice)
	{
		int barrier[] = barriers.get(player),
			pawnInBarrier,
			index;
		
		if(barrier[1] != 0)
			index = 1;
		else if(barrier[0] != 0)
			index = 0;
		else
			index = -1;
		
		if(index != -1)
		{
			pawnInBarrier = ((barrier[index] - barrier[index]%100) - 1)/100;
			
			if(pawns[pawnInBarrier].isAbleToMove(dice,this))
				movePawn(player, pawnInBarrier, dice, m);
		}
		else
			pawnInBarrier = -1;
		
		if(pawnInBarrier != -1)
			return true;
		else
			return false;
	}
	
	void movePawn(Color player, int pawn, int dice, MainFrame m)
	{
		int x,y,nextX,nextY,
			pawnInBarrier,pawnInNextPlace;
		boolean f = false;
		
		x = pawns[pawn].getX()/sideHouses;
		y = pawns[pawn].getY()/sideHouses;
		
		if(pawns[pawn].isAbleToMove(dice, this))
		{
			lastPawn = pawn;
			
			if(grid[x][y]%100 != 0)
			{
				removeBarrier(x*100 + y);
				pawnInBarrier = grid[x][y]%100 -1;
				
				if(pawns[pawnInBarrier].getPlayer() == player)
				{
					int[] barrier = barriers.get(player);
					
					if(barrier[1]%100 == pawn + 1 || (barrier[1] - (barrier[1]%100))/100 == pawn + 1)
					{
						barrier[1] = barrier[0];
						barrier[0] = 0;
					}
					else
						barrier[0] = 0 ;
					
					barriers.put(player, barrier);
				}									
			}
			
			grid[x][y] = (grid[x][y]%100) * 100;
			
			if(pawns[pawn].isHome())
				leftHome(player);
			
			pawns[pawn].move(dice, this);
			
			nextX = (pawns[pawn].getX() - diametrePawns)/sideHouses;
			nextY = (pawns[pawn].getY() - diametrePawns)/sideHouses;
			
			if((nextX != x && nextY != y) || pawns[pawn].getHouse() != 51) 
			{
				if(grid[nextX][nextY] != 0)
				{
					pawnInNextPlace = grid[nextX][nextY]/100 - 1;
					
					if(pawns[pawnInNextPlace].getPlayer() != player)
					{
						for(int i = 0; i < 4; i++)
						{
							if(neutrals.get(MainFrame.players[i]).X/sideHouses == nextX && neutrals.get(MainFrame.players[i]).Y/sideHouses == nextY)
							{
								addBarrier(nextX*100+nextY);										
								grid[nextX][nextY] += pawn + 1;
								f = true;
								break;
							}
							else if(initials.get(MainFrame.players[i]).X/sideHouses == nextX && initials.get(MainFrame.players[i]).Y/sideHouses == nextY)
							{
								addBarrier(nextX*100+nextY);										
								grid[nextX][nextY] += pawn + 1;
								f = true;
								break;
							}
						}
						
						if(!f)
						{
							pawns[pawnInNextPlace].returnHome();
							returnedHome(pawns[pawnInNextPlace].getPlayer());
							grid[(int)Math.floor(pawns[pawnInNextPlace].getX()/sideHouses)][(int)Math.floor(pawns[pawnInNextPlace].getY()/sideHouses)] = (pawnInNextPlace + 1)*100;
							grid[nextX][nextY] = (pawn + 1) * 100;
						}
					}
					else
					{
						int[] barrier = new int[2];
						int pawnOtherBarrier;
						
						addBarrier(nextX*100 + nextY);
						
						barrier = barriers.get(player);
						if(barrier[1] == 0)
							barrier[1] = grid[nextX][nextY] + pawn + 1;
						else
						{
							pawnOtherBarrier = barrier[1]%100 - 1;
							
							if(pawns[pawnOtherBarrier].getHouse() < pawns[pawn].getHouse())
							{
								barrier[0] = barrier[1];
								barrier[1] = grid[nextX][nextY] + pawn + 1;
							}
							else
								barrier[0] = grid[nextX][nextY] + pawn + 1;
						}
						
						grid[nextX][nextY] += pawn + 1;
					}
				}
				else
					grid[nextX][nextY] = (pawn + 1)*100;
			}
					
			updateScore();
			m.Menu.RollDiceButton.setEnabled(true);
			repaint();
			
			if(pawns[pawn].getHouse() == 51)
			{
				arrivedFinal(player);
				lastPawn = -1;

				if(getNumberOfPawnsAtFinal(player) == 4)
				{
					Color[] pl = new Color[4];
					int index = 0;
					pl[index] = player;
					index++;
					
					for(int i = 0; i < 4; i++)
					{
						if(MainFrame.players[i] != player)
						{
							pl[index] = MainFrame.players[i];
							index++;
						}
					}
					
					if(scores.get(pl[1]) < scores.get(pl[2]))
					{
						player = pl[1];
						pl[1] = pl[2];
						pl[2] = player;
					}
					
					if(scores.get(pl[2]) < scores.get(pl[3]))
					{
						player = pl[2];
						pl[2] = pl[3];
						pl[3] = player;
					}
					
					if(scores.get(pl[1]) < scores.get(pl[2]))
					{
						player = pl[1];
						pl[1] = pl[2];
						pl[2] = player;
					}
					
					m.Score.show(pl);
				}
			}
		}
	}
	
	public void addBarrier(int n)
	{
		fullHouses.add(n);
	}
	
	public void removeBarrier(int n)
	{
		Iterator<Integer> iterator = fullHouses.iterator();
		while(iterator.hasNext())
		{
			if(iterator.next() == n)
			{
				iterator.remove();
			}
		}
	}
	
	public void resetPawns(BoardPanel b)
	{
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				pawns[4*i+j].returnHome();
				barriers.put(MainFrame.players[i], new int[]{0,0});
				pawnsAtHome.put(MainFrame.players[j], 4);
				pawnsAtFinal.put(MainFrame.players[j], 0);
			}
		}
		
		fullHouses = new ArrayList<>();
	}
	
	public void leftHome(Color player)
	{
		int i = pawnsAtHome.get(player) - 1;
		pawnsAtHome.put(player, i);
	}
	
	public void returnedHome(Color player)
	{
		int i = pawnsAtHome.get(player) + 1;
		pawnsAtHome.put(player, i);
	}
	
	public void lastReturnHome()
	{
		int x,y;
		if(lastPawn != -1 && pawns[lastPawn].getHouse() != 51)
		{
			x = (int)Math.floor(pawns[lastPawn].getX()/sideHouses);
			y = (int)Math.floor(pawns[lastPawn].getY()/sideHouses);
			grid[x][y] %= 100;
			grid[x][y] *= 100;
			
			x = (int)Math.floor(pawns[lastPawn].getXhome()/sideHouses);
			y = (int)Math.floor(pawns[lastPawn].getYhome()/sideHouses);
			grid[x][y] = (lastPawn + 1) * 100;
			
			pawns[lastPawn].returnHome();
		}
		
		lastPawn = -1;
		repaint();
	}
	
	public void newPlayer()
	{
		lastPawn = -1;
	}
	
	public void arrivedFinal(Color player)
	{
		int i = pawnsAtFinal.get(player) + 1;
		pawnsAtFinal.put(player, i);
	}
	
	public int getNumberOfPawnsAtHome(Color player)
	{
		return pawnsAtHome.get(player);
	}
	
	public int getNumberOfPawnsAtFinal(Color player)
	{
		return pawnsAtFinal.get(player);
	}	
	
	public void updateScore()
	{		
		for(int i = 0; i < 4; i++)
		{
			int score = 0;
			
			for(int j = 0; j < 4; j++)
				score += pawns[4*i+j].getHouse();
			
			scores.put(MainFrame.players[i], score);
		}
	}
	
	boolean isAbleToLeave(Color player)
	{
		int x = initials.get(player).X/sideHouses,
			y = initials.get(player).Y/sideHouses;
		
		if(grid[x][y]%100 == 0 && pawnsAtHome.get(player) > 0)
		{
			if(grid[x][y] == 0)
				return true;
			else if(pawns[grid[x][y]/100 -1].getPlayer() != player)
				return true;
		}
		
		return false;
	}
		
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		// Drawing red side of the board
		g2d.setPaint(MainFrame.RedHouse);
		g2d.fill(new Rectangle2D.Double(0, 0, 6*sideHouses, 6*sideHouses));
		
		g2d.fill(new Rectangle2D.Double(1*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(1*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(2*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(3*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(4*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(5*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		
		g2d.fillPolygon(new int[] {6*sideHouses, (int)(7.5*sideHouses), 6*sideHouses}, new int[] {6*sideHouses, (int)(7.5*sideHouses), 9*sideHouses} , 3);
		
		// Drawing blue side of the board
		g2d.setColor(MainFrame.BlueHouse);
		g2d.fill(new Rectangle2D.Double(0, 9*sideHouses, 6*sideHouses, 6*sideHouses));
		
		g2d.fill(new Rectangle2D.Double(6*sideHouses, 13*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 13*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 10*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 11*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 12*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 9*sideHouses,  sideHouses, sideHouses));
		
		g2d.fillPolygon(new int[] {6*sideHouses, (int)(7.5*sideHouses), 9*sideHouses}, new int[] {9*sideHouses, (int)(7.5*sideHouses), 9*sideHouses} , 3);
		
		// Drawing yellow side of the board
		g2d.setPaint(MainFrame.YellowHouse);
		g2d.fill(new Rectangle2D.Double(9*sideHouses, 9*sideHouses, 6*sideHouses, 6*sideHouses));
		
		g2d.fill(new Rectangle2D.Double(13*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(13*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(12*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(11*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(10*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(9*sideHouses,  7*sideHouses, sideHouses, sideHouses));
		
		g2d.fillPolygon(new int[] {9*sideHouses, (int)(7.5*sideHouses), 9*sideHouses}, new int[] {6*sideHouses, (int)(7.5*sideHouses), 9*sideHouses} , 3);
		
		// Drawing green side of the board
		g2d.setPaint(MainFrame.GreenHouse);
		g2d.fill(new Rectangle2D.Double(9*sideHouses, 0, 6*sideHouses, 6*sideHouses));
		
		g2d.fill(new Rectangle2D.Double(8*sideHouses, 1*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 1*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 2*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 3*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 4*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(7*sideHouses, 5*sideHouses, sideHouses, sideHouses));
		
		g2d.fillPolygon(new int[] {6*sideHouses, (int)(7.5*sideHouses), 9*sideHouses}, new int[] {6*sideHouses, (int)(7.5*sideHouses), 6*sideHouses} , 3);
		
		g2d.setColor(Color.WHITE);
		
		// Drawing entrance houses triangles
		g2d.fillPolygon(new int[] {(int)(1.24*sideHouses), (int)(1.84*sideHouses), (int)(1.24*sideHouses)}, new int[] {(int)(6.2*sideHouses), (int)(6.5*sideHouses), (int)(6.8*sideHouses)} , 3);
		g2d.fillPolygon(new int[] {(int)(8.2*sideHouses), (int)(8.5*sideHouses), (int)(8.8*sideHouses)}, new int[] {(int)(1.24*sideHouses), (int)(1.84*sideHouses), (int)(1.24*sideHouses)} , 3);
		g2d.fillPolygon(new int[] {(int)(6.2*sideHouses), (int)(6.5*sideHouses), (int)(6.8*sideHouses)}, new int[] {(int)(13.8*sideHouses), (int)(13.2*sideHouses), (int)(13.8*sideHouses)} , 3);
		g2d.fillPolygon(new int[] {(int)(13.8*sideHouses), (int)(13.2*sideHouses), (int)(13.8*sideHouses)}, new int[] {(int)(8.2*sideHouses), (int)(8.5*sideHouses), (int)(8.8*sideHouses)} , 3);
		
		// Drawing white circles inside big squares
		Ellipse2D circ = new Ellipse2D.Double();
		
		circ.setFrameFromCenter(pawns[0].getXhome(), pawns[0].getYhome(), pawns[0].getXhome() + diametreHomes, pawns[0].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[1].getXhome(), pawns[1].getYhome(), pawns[1].getXhome() + diametreHomes, pawns[1].getYhome() + diametreHomes);
		g2d.fill(circ);	
		
		circ.setFrameFromCenter(pawns[2].getXhome(), pawns[2].getYhome(), pawns[2].getXhome() + diametreHomes, pawns[2].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[3].getXhome(), pawns[3].getYhome(), pawns[3].getXhome() + diametreHomes, pawns[3].getYhome() + diametreHomes);
		g2d.fill(circ);	
		
		
		circ.setFrameFromCenter(pawns[4].getXhome(), pawns[4].getYhome(), pawns[4].getXhome() + diametreHomes, pawns[4].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[5].getXhome(), pawns[5].getYhome(), pawns[5].getXhome() + diametreHomes, pawns[5].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[6].getXhome(), pawns[6].getYhome(), pawns[6].getXhome() + diametreHomes, pawns[6].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[7].getXhome(), pawns[7].getYhome(), pawns[7].getXhome() + diametreHomes, pawns[7].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		
		circ.setFrameFromCenter(pawns[8].getXhome(), pawns[8].getYhome(), pawns[8].getXhome() + diametreHomes, pawns[8].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[9].getXhome(), pawns[9].getYhome(), pawns[9].getXhome() + diametreHomes, pawns[9].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[10].getXhome(), pawns[10].getYhome(), pawns[10].getXhome() + diametreHomes, pawns[10].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[11].getXhome(), pawns[11].getYhome(), pawns[11].getXhome() + diametreHomes, pawns[11].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		
		circ.setFrameFromCenter(pawns[12].getXhome(), pawns[12].getYhome(), pawns[12].getXhome() + diametreHomes, pawns[12].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[13].getXhome(), pawns[13].getYhome(), pawns[13].getXhome() + diametreHomes, pawns[13].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[14].getXhome(), pawns[14].getYhome(), pawns[14].getXhome() + diametreHomes, pawns[14].getYhome() + diametreHomes);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[15].getXhome(), pawns[15].getYhome(), pawns[15].getXhome() + diametreHomes, pawns[15].getYhome() + diametreHomes);
		g2d.fill(circ);		

		//Drawing black squares                                                               
		g2d.setPaint(Color.BLACK);                                                             
		g2d.fill(new Rectangle2D.Double(6*sideHouses,  sideHouses,    sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(8*sideHouses,  13*sideHouses, sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(sideHouses,    8*sideHouses,  sideHouses, sideHouses));
		g2d.fill(new Rectangle2D.Double(13*sideHouses, 6*sideHouses,  sideHouses, sideHouses));
		
		//Drawing Red pawns
		g2d.setColor(MainFrame.RedPlayer);
		circ.setFrameFromCenter(pawns[0].getX(), pawns[0].getY(), pawns[0].getX() + diametrePawns, pawns[0].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[1].getX(), pawns[1].getY(), pawns[1].getX() + diametrePawns, pawns[1].getY() + diametrePawns);
		g2d.fill(circ);	
		
		circ.setFrameFromCenter(pawns[2].getX(), pawns[2].getY(), pawns[2].getX() + diametrePawns, pawns[2].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[3].getX(), pawns[3].getY(), pawns[3].getX() + diametrePawns, pawns[3].getY() + diametrePawns);
		g2d.fill(circ);	
		
		//Drawing Green pawns		
		g2d.setColor(MainFrame.GreenPlayer);
		circ.setFrameFromCenter(pawns[4].getX(), pawns[4].getY(), pawns[4].getX() + diametrePawns, pawns[4].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[5].getX(), pawns[5].getY(), pawns[5].getX() + diametrePawns, pawns[5].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[6].getX(), pawns[6].getY(), pawns[6].getX() + diametrePawns, pawns[6].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[7].getX(), pawns[7].getY(), pawns[7].getX() + diametrePawns, pawns[7].getY() + diametrePawns);
		g2d.fill(circ);
		
		//Drawing Yellow pawns
		g2d.setColor(MainFrame.YellowPlayer);
		circ.setFrameFromCenter(pawns[8].getX(), pawns[8].getY(), pawns[8].getX() + diametrePawns, pawns[8].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[9].getX(), pawns[9].getY(), pawns[9].getX() + diametrePawns, pawns[9].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[10].getX(), pawns[10].getY(), pawns[10].getX() + diametrePawns, pawns[10].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[11].getX(), pawns[11].getY(), pawns[11].getX() + diametrePawns, pawns[11].getY() + diametrePawns);
		g2d.fill(circ);
		
		//Drawing Blue pawns
		g2d.setColor(MainFrame.BluePlayer);
		circ.setFrameFromCenter(pawns[12].getX(), pawns[12].getY(), pawns[12].getX() + diametrePawns, pawns[12].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[13].getX(), pawns[13].getY(), pawns[13].getX() + diametrePawns, pawns[13].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[14].getX(), pawns[14].getY(), pawns[14].getX() + diametrePawns, pawns[14].getY() + diametrePawns);
		g2d.fill(circ);
		
		circ.setFrameFromCenter(pawns[15].getX(), pawns[15].getY(), pawns[15].getX() + diametrePawns, pawns[15].getY() + diametrePawns);
		g2d.fill(circ);		
		
		// Drawing Edges
		g2d.setPaint(Color.BLACK);   
		g2d.draw(new Rectangle2D.Double(0, 0, 6*sideHouses, 6*sideHouses));
		g2d.drawPolygon(new int[] {6*sideHouses, (int)(7.5*sideHouses), 6*sideHouses}, new int[] {6*sideHouses, (int)(7.5*sideHouses), 9*sideHouses} , 3);
		
		g2d.draw(new Rectangle2D.Double(9*sideHouses, 0, 6*sideHouses, 6*sideHouses));
		g2d.drawPolygon(new int[] {6*sideHouses, (int)(7.5*sideHouses), 9*sideHouses}, new int[] {6*sideHouses, (int)(7.5*sideHouses), 6*sideHouses} , 3);
		
		g2d.draw(new Rectangle2D.Double(9*sideHouses, 9*sideHouses, 6*sideHouses, 6*sideHouses));
		g2d.drawPolygon(new int[] {9*sideHouses, (int)(7.5*sideHouses), 9*sideHouses}, new int[] {9*sideHouses, (int)(7.5*sideHouses), 9*sideHouses} , 3);
		
		g2d.draw(new Rectangle2D.Double(0, 9*sideHouses, 6*sideHouses, 6*sideHouses));
		g2d.drawPolygon(new int[] {6*sideHouses, (int)(7.5*sideHouses), 9*sideHouses}, new int[] {9*sideHouses, (int)(7.5*sideHouses), 9*sideHouses} , 3);
		
		g2d.draw(new Rectangle2D.Double(0*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(0*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(0*sideHouses, 8*sideHouses, sideHouses, sideHouses));		
		g2d.draw(new Rectangle2D.Double(1*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(1*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(1*sideHouses, 8*sideHouses, sideHouses, sideHouses));		
		g2d.draw(new Rectangle2D.Double(2*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(2*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(2*sideHouses, 8*sideHouses, sideHouses, sideHouses));		
		g2d.draw(new Rectangle2D.Double(3*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(3*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(3*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(4*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(4*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(4*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(5*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(5*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(5*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 0*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 0*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 0*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 1*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 1*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 1*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 2*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 2*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 2*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 3*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 3*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 3*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 4*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 4*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 4*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 5*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 5*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 5*sideHouses, sideHouses, sideHouses));
		                                          
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 9*sideHouses,  sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 9*sideHouses,  sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 9*sideHouses,  sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 10*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 10*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 10*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 11*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 11*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 11*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 12*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 12*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 12*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 13*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 13*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 13*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(6*sideHouses, 14*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(7*sideHouses, 14*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(8*sideHouses, 14*sideHouses, sideHouses, sideHouses));
		                                          
		g2d.draw(new Rectangle2D.Double(9*sideHouses,  6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(9*sideHouses,  7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(9*sideHouses,  8*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(10*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(10*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(10*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(11*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(11*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(11*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(12*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(12*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(12*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(13*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(13*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(13*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(14*sideHouses, 6*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(14*sideHouses, 7*sideHouses, sideHouses, sideHouses));
		g2d.draw(new Rectangle2D.Double(14*sideHouses, 8*sideHouses, sideHouses, sideHouses));
		
		circ.setFrameFromCenter(pawns[0].getXhome(), pawns[0].getYhome(), pawns[0].getXhome() + diametreHomes, pawns[0].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[1].getXhome(), pawns[1].getYhome(), pawns[1].getXhome() + diametreHomes, pawns[1].getYhome() + diametreHomes);
		g2d.draw(circ);	
		
		circ.setFrameFromCenter(pawns[2].getXhome(), pawns[2].getYhome(), pawns[2].getXhome() + diametreHomes, pawns[2].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[3].getXhome(), pawns[3].getYhome(), pawns[3].getXhome() + diametreHomes, pawns[3].getYhome() + diametreHomes);
		g2d.draw(circ);	
		
		circ.setFrameFromCenter(pawns[4].getXhome(), pawns[4].getYhome(), pawns[4].getXhome() + diametreHomes, pawns[4].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[5].getXhome(), pawns[5].getYhome(), pawns[5].getXhome() + diametreHomes, pawns[5].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[6].getXhome(), pawns[6].getYhome(), pawns[6].getXhome() + diametreHomes, pawns[6].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[7].getXhome(), pawns[7].getYhome(), pawns[7].getXhome() + diametreHomes, pawns[7].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[8].getXhome(), pawns[8].getYhome(), pawns[8].getXhome() + diametreHomes, pawns[8].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[9].getXhome(), pawns[9].getYhome(), pawns[9].getXhome() + diametreHomes, pawns[9].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[10].getXhome(), pawns[10].getYhome(), pawns[10].getXhome() + diametreHomes, pawns[10].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[11].getXhome(), pawns[11].getYhome(), pawns[11].getXhome() + diametreHomes, pawns[11].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[12].getXhome(), pawns[12].getYhome(), pawns[12].getXhome() + diametreHomes, pawns[12].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[13].getXhome(), pawns[13].getYhome(), pawns[13].getXhome() + diametreHomes, pawns[13].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[14].getXhome(), pawns[14].getYhome(), pawns[14].getXhome() + diametreHomes, pawns[14].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[15].getXhome(), pawns[15].getYhome(), pawns[15].getXhome() + diametreHomes, pawns[15].getYhome() + diametreHomes);
		g2d.draw(circ);
		
		
		circ.setFrameFromCenter(pawns[0].getX(), pawns[0].getY(), pawns[0].getX() + diametrePawns, pawns[0].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[1].getX(), pawns[1].getY(), pawns[1].getX() + diametrePawns, pawns[1].getY() + diametrePawns);
		g2d.draw(circ);	
		
		circ.setFrameFromCenter(pawns[2].getX(), pawns[2].getY(), pawns[2].getX() + diametrePawns, pawns[2].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[3].getX(), pawns[3].getY(), pawns[3].getX() + diametrePawns, pawns[3].getY() + diametrePawns);
		g2d.draw(circ);	

		circ.setFrameFromCenter(pawns[4].getX(), pawns[4].getY(), pawns[4].getX() + diametrePawns, pawns[4].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[5].getX(), pawns[5].getY(), pawns[5].getX() + diametrePawns, pawns[5].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[6].getX(), pawns[6].getY(), pawns[6].getX() + diametrePawns, pawns[6].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[7].getX(), pawns[7].getY(), pawns[7].getX() + diametrePawns, pawns[7].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[8].getX(), pawns[8].getY(), pawns[8].getX() + diametrePawns, pawns[8].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[9].getX(), pawns[9].getY(), pawns[9].getX() + diametrePawns, pawns[9].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[10].getX(), pawns[10].getY(), pawns[10].getX() + diametrePawns, pawns[10].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[11].getX(), pawns[11].getY(), pawns[11].getX() + diametrePawns, pawns[11].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[12].getX(), pawns[12].getY(), pawns[12].getX() + diametrePawns, pawns[12].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[13].getX(), pawns[13].getY(), pawns[13].getX() + diametrePawns, pawns[13].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[14].getX(), pawns[14].getY(), pawns[14].getX() + diametrePawns, pawns[14].getY() + diametrePawns);
		g2d.draw(circ);
		
		circ.setFrameFromCenter(pawns[15].getX(), pawns[15].getY(), pawns[15].getX() + diametrePawns, pawns[15].getY() + diametrePawns);
		g2d.draw(circ);

		// Draw barriers if there is any
		for(int i : fullHouses)
		{
			int x, y, pawnOut, pawnIn;
			y = i%100;
			x = (i - y)/100;
			pawnIn = grid[x][y]%100;
			pawnOut = (grid[x][y] - pawnIn)/100;
			
			pawnIn--;
			pawnOut--;
			
			// house is full but with different players
			if(pawns[pawnOut].getPlayer() != pawns[pawnIn].getPlayer())
			{
				g2d.setColor(pawns[pawnOut].getPlayer());
				circ.setFrameFromCenter(pawns[pawnOut].getX(), pawns[pawnOut].getY(), pawns[pawnOut].getX() + diametrePawns, pawns[pawnOut].getY() + diametrePawns);
				g2d.fill(circ);
				g2d.setColor(pawns[pawnIn].getPlayer());
				circ.setFrameFromCenter(pawns[pawnOut].getX(), pawns[pawnOut].getY(), pawns[pawnOut].getX() + 0.3*sideHouses, pawns[pawnOut].getY() + 0.3*sideHouses);
				g2d.fill(circ);
			}
			// house is full with same player (barrier)
			else
			{
				g2d.setColor(Color.WHITE);
				circ.setFrameFromCenter(pawns[pawnOut].getX(), pawns[pawnOut].getY(), pawns[pawnOut].getX() + 0.36*sideHouses, pawns[pawnOut].getY() + 0.36*sideHouses);
				g2d.fill(circ);
				g2d.setColor(pawns[pawnIn].getPlayer());
				circ.setFrameFromCenter(pawns[pawnOut].getX(), pawns[pawnOut].getY(), pawns[pawnOut].getX() + 0.24*sideHouses, pawns[pawnOut].getY() + 0.24*sideHouses);
				g2d.fill(circ);
			}			
		}
	}
}

