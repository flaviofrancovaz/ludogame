package super_ludo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ScorePanel extends JPanel implements Serializable
{
	private static final long serialVersionUID = 42L;
	private JLabel Vencedor = new JLabel("Vencedor:  ");
	private JLabel Segundo  = new JLabel("Segundo:   ");
	private JLabel Terceiro = new JLabel("Terceiro:  ");
	private JLabel Quarto   = new JLabel("Quarto:    ");
	
	private Color BackGround  = new Color(100, 100, 100);
	private Color BorderColor = new Color(50, 50, 50);
	
	private Color[] classification = new Color[4];
	
	//public Border bo = new Border(1, 5, 1, 1, Color.red);
	
	public ScorePanel(MainFrame m)
	{
		setVisible(false);
		setLayout(null);
		setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, BorderColor));
		setBounds((int)(4.6*BoardPanel.sideHouses)+1, (int)(4*BoardPanel.sideHouses), (int)(7*BoardPanel.sideHouses), (int)(8*BoardPanel.sideHouses)+1);
		setBackground(BackGround);
		
		Vencedor.setBounds((int)(BoardPanel.sideHouses), (int)(0.8*BoardPanel.sideHouses), (int)(6.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		Vencedor.setForeground(Color.BLACK);
		Vencedor.setFont(new Font(Font.MONOSPACED, Font.BOLD, (int)(0.46*BoardPanel.sideHouses)));
		
		Segundo.setBounds((int)(BoardPanel.sideHouses), (int)(2.5*BoardPanel.sideHouses), (int)(6.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		Segundo.setForeground(Color.BLACK);
		Segundo.setFont(new Font(Font.MONOSPACED, Font.BOLD, (int)(0.4*BoardPanel.sideHouses)));
		
		Terceiro.setBounds((int)(BoardPanel.sideHouses), (int)(4.5*BoardPanel.sideHouses), (int)(6.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		Terceiro.setForeground(Color.BLACK);
		Terceiro.setFont(new Font(Font.MONOSPACED, Font.BOLD, (int)(0.4*BoardPanel.sideHouses)));
		
		Quarto.setBounds((int)(BoardPanel.sideHouses), (int)(6.5*BoardPanel.sideHouses), (int)(6.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		Quarto.setForeground(Color.BLACK);
		Quarto.setFont(new Font(Font.MONOSPACED, Font.BOLD, (int)(0.4*BoardPanel.sideHouses)));
		
		add(Vencedor);
		add(Segundo);
		add(Terceiro);
		add(Quarto);
	}
	
	public void show(Color[] players)
	{		
		for(int i = 0; i < 4; i++)
			classification[i] = players[i];
		
		setVisible(true);
		repaint();		
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		Rectangle2D rec = new Rectangle2D.Double();
		
		g2d.setPaint(classification[0]);
		rec.setFrame((int)(4*BoardPanel.sideHouses), (int)(0.8*BoardPanel.sideHouses), (int)(2.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		g2d.fill(rec);
		
		for(int i = 1; i < 4; i++)
		{
			g2d.setPaint(classification[i]);
			rec.setFrame((int)(4*BoardPanel.sideHouses), (int)(2.6*BoardPanel.sideHouses + (i-1)*2*BoardPanel.sideHouses), (int)(2.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
			g2d.fill(rec);
		}
	}
}
