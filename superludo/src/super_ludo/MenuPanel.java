package super_ludo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import java.awt.event.MouseEvent;

public class MenuPanel extends JPanel implements Serializable
{
	private static final long serialVersionUID = 42L;
	private static Image[] Dice = new Image[6];
	private static int diceX = (int)(0.9*BoardPanel.sideHouses);
	private static int diceY  = (int)(7.9*BoardPanel.sideHouses);
	private static Color BlueButton = new Color(89, 0, 179);
	private static Color HackButton = new Color(230, 230, 230);
	
	private JButton NewGameButton  = new JButton("Novo Jogo");
	private JButton LoadGameButton = new JButton("Carregar Jogo");
	private JButton SaveGameButton = new JButton("Salvar Jogo");	
	public  JButton RollDiceButton = new JButton("Lan�ar Dado");
	
	private JButton HackDice1 = new JButton("1");
	private JButton HackDice2 = new JButton("2");
	private JButton HackDice3 = new JButton("3");
	private JButton HackDice4 = new JButton("4");
	private JButton HackDice5 = new JButton("5");
	private JButton HackDice6 = new JButton("6");
	private int hack = 0;
	 
	private JLabel Label = new JLabel("A Jogar:");
	
	private int dice;
	private Color currentPlayer;
	private char repeat = '0';
	
	public MenuPanel(MainFrame m) 
	{	
		LoadDiceImages();
		currentPlayer = m.getCurrentPlayer();
		dice = -1;
				
		setVisible(true);
		setLayout(null);
		setBounds(15*BoardPanel.sideHouses+1, 0, 4*BoardPanel.sideHouses, 15*BoardPanel.sideHouses+1);
		setBackground(MainFrame.BlueMenu);
		
		NewGameButton.setBounds((int)(0.6*BoardPanel.sideHouses), BoardPanel.sideHouses, (int)(2.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		NewGameButton.setBackground(BlueButton);
		NewGameButton.setForeground(Color.WHITE);
		NewGameButton.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		NewGameButton.addActionListener(new ButtonListener());

		NewGameButton.addMouseListener(new MouseAdapter()
				{
					public void mouseClicked(MouseEvent e)
					{
						dice = -1;
						m.restart();
						m.Board.repaint();
						repaint();
						currentPlayer = m.getCurrentPlayer();
						RollDiceButton.setEnabled(true);
					}
				});
		
		LoadGameButton.setBounds((int)(0.6*BoardPanel.sideHouses), (int)(2.4*BoardPanel.sideHouses), (int)(2.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		LoadGameButton.setBackground(BlueButton);
		LoadGameButton.setForeground(Color.WHITE);
		LoadGameButton.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		
		LoadGameButton.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				final JFileChooser fc = new JFileChooser();
			
				//In response to a button click:
				File f = null;
				
				while(true)
				{
					int userSelection = fc.showOpenDialog(m);
				
					if (userSelection == JFileChooser.APPROVE_OPTION) 
					{
						f = fc.getSelectedFile();
						if(f.exists())
							break;
						else
							f = null;
					}
					else if(userSelection == JFileChooser.CANCEL_OPTION)
					{
						f = null;
						break;
					}
				}
				
				if(f != null)
				{
					try
					{						
						FileInputStream fileToLoad = new FileInputStream(f.getPath());
					    ObjectInputStream s = new ObjectInputStream(fileToLoad);
					    
					    MainFrame.LoadGame(s);
					    s.close();
					    fileToLoad.close();

					    m.Board.repaint();
					    m.Menu.repaint();
					}
					catch(FileNotFoundException ex) 
					{
						System.out.println("File not found");
					} 
					catch(IOException ex) 
					{
						System.out.println("Error initializing stream");
					}
				}
			}
		});

		SaveGameButton.setBounds((int)(0.6*BoardPanel.sideHouses), (int)(3.8*BoardPanel.sideHouses), (int)(2.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		SaveGameButton.setBackground(BlueButton);
		SaveGameButton.setForeground(Color.WHITE);
		SaveGameButton.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		
		SaveGameButton.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				final JFileChooser fc = new JFileChooser();
				UIManager.put("Synthetica.extendedFileChooser.rememberPreferences", Boolean.FALSE);
				//In response to a button click:
				int userSelection = fc.showSaveDialog(m);
				
				if (userSelection == JFileChooser.APPROVE_OPTION)
				{
					try
					{

						// save 
						FileOutputStream fileToSave = new FileOutputStream(fc.getSelectedFile().getPath());
					    ObjectOutputStream s = new ObjectOutputStream(fileToSave);
					    
					    s.writeObject(m);
					    s.close();
					    fileToSave.close();
					}
					catch (FileNotFoundException ex) 
					{
						System.out.println("File not found");
					} 
					catch (Exception ex) 
					{
						System.out.println(ex.getMessage());
					}
				}
			}
		});
		
		add(NewGameButton);
		add(LoadGameButton);
		add(SaveGameButton);
		
		Label.setBounds((int)(1.4*BoardPanel.sideHouses), (int)(6.6*BoardPanel.sideHouses), (int)(2.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		Label.setForeground(Color.WHITE);
		Label.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		
		add(Label);
		
		RollDiceButton.setBounds((int)(0.6*BoardPanel.sideHouses), (int)(10.6*BoardPanel.sideHouses), (int)(2.6*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		RollDiceButton.setBackground(BlueButton);
		RollDiceButton.setForeground(Color.WHITE);
		RollDiceButton.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		RollDiceButton.addActionListener(new ButtonListener());
		
		add(RollDiceButton);
		
		RollDiceButton.addMouseListener(new MouseAdapter()
				{
					public void mouseClicked(MouseEvent e)
					{
						if(RollDiceButton.isEnabled())
						{
							// previous player
							currentPlayer = m.getCurrentPlayer();
							RollDiceButton.setEnabled(false);
							
							// initializes first player
							if(currentPlayer == MainFrame.BlueMenu)
								m.setInitialPlayer();
							else
								m.nextPlayer(dice,repeat);
							
							if(currentPlayer != m.getCurrentPlayer())
								m.Board.newPlayer();
							
							updateRepeat();
							
							// real current player
							currentPlayer = m.getCurrentPlayer();
							
							// ignores players that had already finished
							if(m.Board.getNumberOfPawnsAtFinal(currentPlayer) == 4)
							{
								dice = -1;
								while(m.Board.getNumberOfPawnsAtFinal(currentPlayer) == 4)
								{
									repeat = '0';
									m.nextPlayer(dice,repeat);
									currentPlayer = m.getCurrentPlayer();
								}
							}
							
							if(hack != 0)
								dice = hack;
							else
								setDiceResult();
							
							if(dice == 6)
							{
								if(repeat == '2')
								{
									m.Board.lastReturnHome();
									m.Board.returnedHome(currentPlayer);
									RollDiceButton.setEnabled(true);
								}
								else if(m.Board.rollSix(m, currentPlayer, dice))
									RollDiceButton.setEnabled(true);
							}
							
							if(!RollDiceButton.isEnabled() && !isAbleToMakeAMove(m.Board, currentPlayer, dice))
							//if(dice != 5 && (m.Board.numberOfPawnsAtHome(currentPlayer) + m.Board.numberOfPawnsAtFinal(currentPlayer)) ==  4)
								RollDiceButton.setEnabled(true);
							
							hack = 0;
							HackDice1.setEnabled(true);
							HackDice2.setEnabled(true);
							HackDice3.setEnabled(true);
							HackDice4.setEnabled(true);
							HackDice5.setEnabled(true);
							HackDice6.setEnabled(true);
							
							repaint();
						}
					}
				});		
	
		HackDice1.setBounds((int)(0.5*BoardPanel.sideHouses), 12*BoardPanel.sideHouses, (int)(0.9*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		HackDice1.setBackground(HackButton);
		HackDice1.setForeground(Color.BLACK);
		HackDice1.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		HackDice1.addActionListener(new ButtonListener());
		HackDice1.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				hack = 1;
				HackDice1.setEnabled(false);
				HackDice2.setEnabled(true);
				HackDice3.setEnabled(true);
				HackDice4.setEnabled(true);
				HackDice5.setEnabled(true);
				HackDice6.setEnabled(true);
			}
		});
		
		HackDice2.setBounds((int)(1.5*BoardPanel.sideHouses), 12*BoardPanel.sideHouses, (int)(0.9*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		HackDice2.setBackground(HackButton);
		HackDice2.setForeground(Color.BLACK);
		HackDice2.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		HackDice2.addActionListener(new ButtonListener());
		HackDice2.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				hack = 2;
				HackDice2.setEnabled(false);
				HackDice1.setEnabled(true);
				HackDice3.setEnabled(true);
				HackDice4.setEnabled(true);
				HackDice5.setEnabled(true);
				HackDice6.setEnabled(true);
			}
		});
		
		HackDice3.setBounds((int)(2.5*BoardPanel.sideHouses), 12*BoardPanel.sideHouses, (int)(0.9*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		HackDice3.setBackground(HackButton);
		HackDice3.setForeground(Color.BLACK);
		HackDice3.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		HackDice3.addActionListener(new ButtonListener());
		HackDice3.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				hack = 3;
				HackDice3.setEnabled(false);
				HackDice1.setEnabled(true);
				HackDice2.setEnabled(true);
				HackDice4.setEnabled(true);
				HackDice5.setEnabled(true);
				HackDice6.setEnabled(true);
			}
		});
		
		HackDice4.setBounds((int)(0.5*BoardPanel.sideHouses), (int)(12.7*BoardPanel.sideHouses), (int)(0.9*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		HackDice4.setBackground(HackButton);
		HackDice4.setForeground(Color.BLACK);
		HackDice4.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		HackDice4.addActionListener(new ButtonListener());
		HackDice4.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				hack = 4;
				HackDice4.setEnabled(false);
				HackDice1.setEnabled(true);
				HackDice2.setEnabled(true);
				HackDice3.setEnabled(true);
				HackDice5.setEnabled(true);
				HackDice6.setEnabled(true);
			}
		});
		
		HackDice5.setBounds((int)(1.5*BoardPanel.sideHouses), (int)(12.7*BoardPanel.sideHouses), (int)(0.9*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		HackDice5.setBackground(HackButton);
		HackDice5.setForeground(Color.BLACK);
		HackDice5.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		HackDice5.addActionListener(new ButtonListener());
		HackDice5.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				hack = 5;
				HackDice5.setEnabled(false);
				HackDice1.setEnabled(true);
				HackDice2.setEnabled(true);
				HackDice3.setEnabled(true);
				HackDice4.setEnabled(true);
				HackDice6.setEnabled(true);
			}
		});
		
		HackDice6.setBounds((int)(2.5*BoardPanel.sideHouses), (int)(12.7*BoardPanel.sideHouses), (int)(0.9*BoardPanel.sideHouses), (int)(0.6*BoardPanel.sideHouses));
		HackDice6.setBackground(HackButton);
		HackDice6.setForeground(Color.BLACK);
		HackDice6.setFont(new Font("Dialog", Font.BOLD, (int)(0.24*BoardPanel.sideHouses)));
		HackDice6.addActionListener(new ButtonListener());
		HackDice6.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				hack = 6;
				HackDice6.setEnabled(false);
				HackDice1.setEnabled(true);
				HackDice2.setEnabled(true);
				HackDice3.setEnabled(true);
				HackDice4.setEnabled(true);
				HackDice5.setEnabled(true);
			}
		});
		
		add(HackDice1);
		add(HackDice2);
		add(HackDice3);
		add(HackDice4);
		add(HackDice5);
		add(HackDice6);
		
		HackDice1.setVisible(true);
		HackDice2.setVisible(true);
		HackDice3.setVisible(true);
		HackDice4.setVisible(true);
		HackDice5.setVisible(true);
		HackDice6.setVisible(true);	
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		double topX = 0.6*BoardPanel.sideHouses;
		double topY = 7.6*BoardPanel.sideHouses;
		
		double squareWidth  = 2.6*BoardPanel.sideHouses;
		double squareHeight = 2.6*BoardPanel.sideHouses;
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.setColor(currentPlayer);
		g2d.fill(new Rectangle2D.Double(topX, topY, squareWidth, squareHeight));	
		
		if(dice > -1)
			g2d.drawImage(Dice[dice-1], diceX, diceY, null);
	}

	private void updateRepeat()
	{
		if(dice == 6)
		{
			if(repeat == '0')
				repeat = '1';
			else if(repeat == '1')
				repeat = '2';
			else
				repeat = '0';
		}
		else
			repeat = '0';
	}
	
	private void setDiceResult()
	{	
		dice = new Random().nextInt(6) + 1;		
	}
	
	private void LoadDiceImages()
	{
		try 
		{
          	Dice[0] = ImageIO.read(new File("Dices/Dado1.png"));
			Dice[1] = ImageIO.read(new File("Dices/Dado2.png"));
			Dice[2] = ImageIO.read(new File("Dices/Dado3.png"));
			Dice[3] = ImageIO.read(new File("Dices/Dado4.png"));
			Dice[4] = ImageIO.read(new File("Dices/Dado5.png"));
			Dice[5] = ImageIO.read(new File("Dices/Dado6.png"));
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public int getDice()
	{
		return dice;
	}
	
	public boolean isAbleToMakeAMove(BoardPanel b, Color player, int dice)
	{
		int p;
		
		if(player == MainFrame.RedPlayer)
			p = 0;
		else if(player == MainFrame.GreenPlayer)
			p = 4;
		else if(player == MainFrame.YellowPlayer)
			p = 8;
		else if(player == MainFrame.BluePlayer) 
			p = 12;
		else
			p = -1;
		
		if(p != -1)
		{
			for(int i = 0; i < 4; i++)
			{
				if(b.pawns[p+i].isAbleToMove(dice, b))
						return true;
			}
		}

		return false;
	}

}
