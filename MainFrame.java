package superludo;
import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame
{
	private static final long serialVersionUID = 1L;
	public final int LARG_DEFAULT	=	1100;
	public final int ALT_DEFAULT	=	935;	
	
	JButton NewGameButton  = new JButton("Novo Jogo");
	JButton LoadGameButton = new JButton("Carregar Jogo");
	JButton SaveGameButton = new JButton("Salvar Jogo");
	
	private static MenuPanel Menu   = new MenuPanel();
	private BoardPanel Board = new BoardPanel();
	
	public MainFrame() 
	{
		setLayout(null);
		Toolkit tk=Toolkit.getDefaultToolkit();
		Dimension screenSize=tk.getScreenSize();
		
		int sl=screenSize.width;
		int sa=screenSize.height;
		
		int x = sl/2 - LARG_DEFAULT/2;
		int y = sa/2 - ALT_DEFAULT/2;
		
		setBounds(x,y,LARG_DEFAULT,ALT_DEFAULT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Super Ludo");
		setVisible(true);
		setResizable(false);
		
		getContentPane().add(Menu);
		getContentPane().add(Board);
	}
}
